#include "Wave.h"
#include "Random.h"

#include <fmt/core.h>

void Wave::regenerate(WaveGenerationParams const& params)
{
    fmt::print("Regenerating wave\n");
    float amplitude = params.starting_amplitude;
    float frequency = params.starting_frequency;

    for (size_t i = 0; i < NUM_WAVES; ++i) {
        amplitudes[i] = Random::uniform_float(0.1f, 1.f) * amplitude;
        frequencies[i] = Random::uniform_float(0.1f, 1.f) * frequency;
        speeds[i] = Random::uniform_float(params.minimum_speed, params.maximum_speed);
        bearings[i] = Random::normal_float(params.bearing_mean, params.bearing_stddev);

        amplitude *= params.amplitude_coefficient;
        frequency *= params.frequency_coefficient;
    }
}

Wave::Wave()
{
    // Generate with default parameters
    regenerate(WaveGenerationParams());
}
