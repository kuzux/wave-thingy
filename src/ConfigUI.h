#pragma once

#include "Wave.h"

#include "vendor/imgui.h"
#include <glm/glm.hpp>

class ConfigUI
{
public:
    ConfigUI() { }
    ~ConfigUI() { }

    void draw();

    bool show_wireframe() const { return m_show_wireframe; }
    glm::vec3 camera_pos() const { return m_camera_pos; }
    glm::vec3 camera_dir_spherical() const { return m_camera_dir_spherical; }
    Wave const& wave() const { return m_wave; }
    glm::vec3 wave_color() const { return m_wave_color; }
    int scene_id() const { return m_scene_id; }

private:
    WaveGenerationParams m_wave_gen_params;
    Wave m_wave;

    int m_scene_id { 0 };

    glm::vec3 m_wave_color { .1f, .5f, .6f };
    bool m_show_wireframe { false };
    glm::vec3 m_camera_pos { 0.f, 0.f, 1.f };
    // r, theta, phi
    glm::vec3 m_camera_dir_spherical { 1.f, 1.05f, 0.f };
};
