#include "ConfigUI.h"

#include "vendor/imgui.h"
#include "vendor/imgui_impl_opengl3.h"
#include "vendor/imgui_impl_sdl2.h"

void ConfigUI::draw()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame();
    ImGui::NewFrame();

    ImGui::Begin("Config");

    if (ImGui::TreeNode("Wave parameters")) {
        ImGui::SliderFloat("Starting Amplitude", &m_wave_gen_params.starting_amplitude, 0.1f, 1.5f);
        ImGui::SliderFloat(
            "Starting Frequency", &m_wave_gen_params.starting_frequency, 0.01f, 0.5f);

        ImGui::SliderFloat(
            "Amplitude Coefficient", &m_wave_gen_params.amplitude_coefficient, 0.1f, 1.f);
        ImGui::SliderFloat(
            "Frequency Coefficient", &m_wave_gen_params.frequency_coefficient, 1.f, 2.f);

        ImGui::SliderFloat("Minimum Speed", &m_wave_gen_params.minimum_speed, 0.1f, 1.f);
        ImGui::SliderFloat("Maximum Speed", &m_wave_gen_params.maximum_speed, .5f, 2.f);

        ImGui::SliderAngle("Bearing Mean", &m_wave_gen_params.bearing_mean, 0.f, 360.f);
        ImGui::SliderAngle("Bearing Stddev", &m_wave_gen_params.bearing_stddev, 0.f, 30.f);

        if (ImGui::Button("Regenerate Wave"))
            m_wave.regenerate(m_wave_gen_params);

        ImGui::TreePop();
    }

    ImGui::RadioButton("Wave Scene", &m_scene_id, 0);
    ImGui::SameLine();
    ImGui::RadioButton("Graph Scene", &m_scene_id, 1);

    ImGui::Checkbox("Show Wireframe", &m_show_wireframe);

    ImGui::ColorEdit3("Wave Color", &m_wave_color.x, ImGuiColorEditFlags_Float);

    ImGui::SliderFloat("Camera X", &m_camera_pos.x, -5.f, 5.f);
    ImGui::SliderFloat("Camera Y", &m_camera_pos.y, -5.f, 5.f);
    ImGui::SliderFloat("Camera Z", &m_camera_pos.z, 0.f, 5.f);

    ImGui::SliderFloat("Look up/down", &m_camera_dir_spherical.y, -3.14f, 3.14f);
    ImGui::SliderFloat("Look right/left", &m_camera_dir_spherical.z, -3.14f, 3.14f);

    ImGui::End();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
