#pragma once

#include <functional>
#include <string>
#include <string_view>
#include <unordered_map>

#include "Error.h"

#include <SDL.h>
#include <glm/vec2.hpp>

// clang-format off
#define USE_IMGUI
#define IMGUI_HEADER(h) vendor/h
// clang-format on

class SDLEventLoop;
class SDLWindow
{
    friend class SDLEventLoop;

public:
    SDLWindow(std::string_view title, glm::ivec2 size);
    ~SDLWindow();

    void draw();
    glm::ivec2 window_size() const { return m_window_size; }
    float aspect_ratio() const { return (float)m_window_size.x / (float)m_window_size.y; }

    void set_window_size(glm::ivec2 size) { m_window_size = size; }
    void set_title(std::string_view title);
    void set_icon(std::string const& filename);
    ErrorOr<void> setup_imgui();
    SDLEventLoop& event_loop();

private:
    SDL_Window* window = nullptr;
    SDL_GLContext ctx;

    glm::ivec2 m_window_size;
    SDL_Surface* m_window_icon { nullptr };
    SDLEventLoop* m_event_loop { nullptr };
    bool m_vsynced { false };
};

class SDLEventLoop
{
    friend class SDLWindow;

public:
    void run(std::function<void(uint64_t)> body);
    void register_event(SDL_EventType type, std::function<void(SDL_Event)> callback);
    void stop();

private:
    SDLEventLoop(SDLWindow& window);

    bool m_running { false };
    std::unordered_map<SDL_EventType, std::function<void(SDL_Event)>> m_event_cbs;
    SDLWindow& m_window;
};
