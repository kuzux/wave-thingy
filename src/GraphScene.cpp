#include "GraphScene.h"

#include <array>

#include "ConfigUI.h"
#include "GLArrayBuffer.h"
#include "GLShader.h"
#include "GLVertexArray.h"

#include "GLFramework.h"

using namespace std;

struct VertexInfo {
    glm::vec3 pos;
    glm::vec2 uv;
};

// clang-format off
static array<VertexInfo, 6> vertex_buffer_data = {{
    { { -1.f, -1.f, 0.f }, { 0.f, 0.f } },
    { {  1.f, -1.f, 0.f }, { 1.f, 0.f } },
    { {  1.f,  1.f, 0.f }, { 1.f, 1.f } },
    { { -1.f, -1.f, 0.f }, { 0.f, 0.f } },
    { {  1.f,  1.f, 0.f }, { 1.f, 1.f } },
    { { -1.f,  1.f, 0.f }, { 0.f, 1.f } },
}};
// clang-format on

struct GraphScene::Impl {
    GLProgram program;
    GLArrayBuffer<VertexInfo> vertex_buffer { GLBufferTarget::ArrayBuffer };
    GLVertexArray vertex_array;

    void setup();
    void draw(ConfigUI const& config, float secs);
};

void GraphScene::Impl::setup()
{
    {
        auto binding = vertex_buffer.bind();
        vertex_buffer.load(vertex_buffer_data);

        auto array_binding = vertex_array.bind();
        vertex_array.define_attribute(0, GLVertexAttribute(glm::vec3, VertexInfo, pos));
        vertex_array.define_attribute(1, GLVertexAttribute(glm::vec2, VertexInfo, uv));
    }

    auto vertex_shader
        = MUST(GLShader::from_file("assets/graph.vert.glsl", GLShader::Type::Vertex));
    auto fragment_shader
        = MUST(GLShader::from_file("assets/graph.frag.glsl", GLShader::Type::Fragment));
    program.attach(vertex_shader);
    program.attach(fragment_shader);
    MUST(program.link());
}

void GraphScene::Impl::draw(ConfigUI const& config, float secs)
{
    auto const& wave = config.wave();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto shader_binding = program.bind();
    auto binding = vertex_array.bind();
    program.get_uniform<float>("time").set_value(secs);

    program.get_uniform<Wave::Param>("amplitudes").set_value(wave.amplitudes);
    program.get_uniform<Wave::Param>("frequencies").set_value(wave.frequencies);
    program.get_uniform<Wave::Param>("speeds").set_value(wave.speeds);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glDrawArrays(GL_TRIANGLES, 0, 6);
}

GraphScene::GraphScene(ConfigUI const& config)
    : m_impl(new GraphScene::Impl())
    , m_config(config)
{
    m_impl->setup();
}

GraphScene::~GraphScene() { delete m_impl; }

void GraphScene::draw(float secs) { m_impl->draw(m_config, secs); }
