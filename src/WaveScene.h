#pragma once

#include "ConfigUI.h"
#include "Scene.h"

class WaveScene : public Scene
{
    struct Impl;

public:
    WaveScene(ConfigUI const& config, float aspect_ratio);
    ~WaveScene();

    void draw(float secs) override;

private:
    Impl* m_impl;
    ConfigUI const& m_config;
};
