#pragma once

#include <array>
#include <glm/glm.hpp>

struct WaveGenerationParams {
    // The waves are sinusoid functions, amplitudes are in meters
    // Frequencies are in wavelengths per second
    float starting_amplitude { .6f };
    float starting_frequency { .08f };

    // In fractional brownian motion, the amplitude and frequency of each successive wave
    // is multiplied by a constant. The amplitude coefficient is <1, and the frequency
    // coefficient is >1.
    float amplitude_coefficient { .75f };
    float frequency_coefficient { 1.18f };

    // Speeds are uniformly distributed and are in wavelengths per second
    float minimum_speed { .25f };
    float maximum_speed { 1.f };

    // The bearing is the angle between the wave's direction of propagation and the x-axis
    // Bearings are normally distributed and are in radians
    float bearing_mean { 0.785398 }; // PI/4
    float bearing_stddev { .4f };
};

struct Wave {
    constexpr static int NUM_WAVES = 40;
    using Param = std::array<float, NUM_WAVES>;

    Param amplitudes;
    Param frequencies;
    Param speeds;
    Param bearings;

    Wave();
    ~Wave() { }
    void regenerate(WaveGenerationParams const& params);
};
