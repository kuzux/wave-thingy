#pragma once

#include <array>
#include <glm/glm.hpp>
#include <string>

#include "Error.h"
#include "GLCubemap.h"
#include "GLTypes.h"

class Skybox
{
    struct Impl;

public:
    struct Binding {
        friend class Skybox;
        Binding(Skybox& cubemap);
        ~Binding();

    private:
        Skybox& m_skybox;
    };

    Skybox();
    ~Skybox();

    // shader filename order: vertex, fragment
    // TODO: Embed the shaders into the code and not deal with shader files
    ErrorOr<void> load(GLCubemap& cubemap, std::array<std::string, 2> shader_filenames);

    void set_projection_matrix(glm::mat4 const& projection_matrix);
    void set_camera_matrix(glm::mat4 const& camera_matrix);

    // binds the models, the shader and the texture
    [[nodiscard]] Binding bind();
    void draw();

    GLCubemap const& cubemap() const;

private:
    Impl* m_impl { nullptr };

    bool m_is_loaded { false };
    bool m_is_bound { false };
};
