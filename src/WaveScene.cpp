#include "WaveScene.h"

#include <array>
#include <vector>

#include "GLArrayBuffer.h"
#include "GLShader.h"
#include "GLVertexArray.h"
#include "SDLWindow.h"
#include "Skybox.h"

#include "ConfigUI.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "GLFramework.h"
using namespace std;

struct VertexInfo {
    glm::vec3 position;
};

struct WaveScene::Impl {
    vector<VertexInfo> vertex_buffer_data;
    vector<GL::uint> element_buffer_data;

    GLArrayBuffer<VertexInfo> vertex_buffer { GLBufferTarget::ArrayBuffer };
    GLArrayBuffer<GL::uint> element_buffer { GLBufferTarget::ElementArrayBuffer };

    GLProgram program;
    GLProgram wireframe_program;

    GLVertexArray vertex_array;
    GLCubemap cubemap;
    Skybox skybox;

    void generate_plane(size_t rows, size_t columns);
    void setup(float aspect_ratio);

    glm::mat4 compute_camera_matrix(glm::vec3 camera_pos, glm::vec3 camera_dir_spherical);
    void draw(ConfigUI const& config, float secs);
};

void WaveScene::Impl::generate_plane(size_t rows, size_t columns)
{
    float dx = .1f;
    float x0 = -50.f;
    float y0 = -50.f;

    // Set up vertices
    for (size_t r = 0; r < rows; ++r) {
        for (size_t c = 0; c < columns; ++c) {
            vertex_buffer_data.push_back(VertexInfo { glm::vec3(x0 + c * dx, y0 + r * dx, 0.0) });
        }
    }

    // Set up indices
    for (size_t r = 0; r < rows - 1; ++r) {
        for (size_t c = 0; c < columns - 1; ++c) {
            element_buffer_data.push_back(r * columns + c);
            element_buffer_data.push_back(r * columns + (c + 1));
            element_buffer_data.push_back((r + 1) * columns + c);

            element_buffer_data.push_back((r + 1) * columns + c);
            element_buffer_data.push_back(r * columns + (c + 1));
            element_buffer_data.push_back((r + 1) * columns + (c + 1));
        }
    }
}

void WaveScene::Impl::setup(float aspect_ratio)
{
    generate_plane(1000, 1000);
    MUST(cubemap.load({
        "assets/clouds1_east.png",
        "assets/clouds1_west.png",
        "assets/clouds1_up.png",
        "assets/clouds1_down.png",
        "assets/clouds1_north.png",
        "assets/clouds1_south.png",
    }));
    MUST(skybox.load(cubemap,
        {
            "assets/skybox.vert.glsl",
            "assets/skybox.frag.glsl",
        }));

    {
        auto binding = vertex_buffer.bind();
        vertex_buffer.load(vertex_buffer_data);

        auto array_binding = vertex_array.bind();
        vertex_array.define_attribute(0, GLVertexAttribute(glm::vec3, VertexInfo, position));
    }

    {
        auto binding = element_buffer.bind();
        element_buffer.load(element_buffer_data);
    }

    auto vertex_shader_path = "assets/vertex.glsl";
    auto fragment_shader_path = "assets/fragment.glsl";

    auto vertex_shader = MUST(GLShader::from_file(vertex_shader_path, GLShader::Type::Vertex));
    auto fragment_shader
        = MUST(GLShader::from_file(fragment_shader_path, GLShader::Type::Fragment));
    auto wireframe_fragment_shader
        = MUST(GLShader::from_file("assets/wireframe.glsl", GLShader::Type::Fragment));

    program.attach(vertex_shader);
    program.attach(fragment_shader);

    wireframe_program.attach(vertex_shader);
    wireframe_program.attach(wireframe_fragment_shader);

    MUST(program.link());
    MUST(wireframe_program.link());

    glm::mat4 projection_mat = glm::perspective(glm::radians(90.f), aspect_ratio, 0.1f, 100.f);
    glm::vec3 light_pos = glm::vec3(0.f, 0.f, 10.f);

    {
        auto shader_binding = program.bind();
        program.get_uniform<glm::mat4>("projection").set_value(projection_mat);
        program.get_uniform<glm::vec3>("light_pos").set_value(light_pos);
    }
    {
        auto shader_binding = wireframe_program.bind();
        wireframe_program.get_uniform<glm::mat4>("projection").set_value(projection_mat);
    }
    {
        auto binding = skybox.bind();
        skybox.set_projection_matrix(projection_mat);
    }
}

WaveScene::WaveScene(ConfigUI const& config, float aspect_ratio)
    : m_impl(new WaveScene::Impl())
    , m_config(config)
{
    m_impl->setup(aspect_ratio);
}

WaveScene::~WaveScene() { delete m_impl; }

glm::mat4 WaveScene::Impl::compute_camera_matrix(
    glm::vec3 camera_pos, glm::vec3 camera_dir_spherical)
{
    glm::vec3 camera_up = glm::vec3(0.f, 0.f, 1.f);
    float r = camera_dir_spherical.x;
    float theta = camera_dir_spherical.y;
    float phi = camera_dir_spherical.z;

    glm::vec3 camera_dir
        = glm::vec3(r * sin(theta) * cos(phi), r * sin(theta) * sin(phi), r * cos(theta));
    glm::vec3 camera_focus = camera_pos + camera_dir;
    return glm::lookAt(camera_pos, camera_focus, camera_up);
}

void WaveScene::Impl::draw(ConfigUI const& config, float secs)
{
    auto camera_pos = config.camera_pos();
    auto camera_mat = compute_camera_matrix(camera_pos, config.camera_dir_spherical());
    auto const& wave = config.wave();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    {
        auto binding = skybox.bind();
        skybox.set_camera_matrix(camera_mat);
        skybox.draw();
    }

    auto binding = vertex_array.bind();
    auto element_binding = element_buffer.bind();

    auto shader_binding = program.bind();
    auto cubemap_binding = cubemap.bind();

    program.get_uniform<glm::mat4>("camera").set_value(camera_mat);
    program.get_uniform<glm::vec3>("camera_pos").set_value(camera_pos);

    program.get_uniform<float>("time").set_value(secs);
    program.get_uniform<Wave::Param>("amplitudes").set_value(wave.amplitudes);
    program.get_uniform<Wave::Param>("frequencies").set_value(wave.frequencies);
    program.get_uniform<Wave::Param>("speeds").set_value(wave.speeds);
    program.get_uniform<Wave::Param>("bearings").set_value(wave.bearings);

    program.get_uniform<glm::vec3>("wave_color").set_value(config.wave_color());

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDrawElements(GL_TRIANGLES, element_buffer_data.size(), GL_UNSIGNED_INT, (void*)0);

    if (config.show_wireframe()) {
        auto shader_binding = wireframe_program.bind();

        wireframe_program.get_uniform<glm::mat4>("camera").set_value(camera_mat);

        wireframe_program.get_uniform<float>("time").set_value(secs);
        wireframe_program.get_uniform<Wave::Param>("amplitudes").set_value(wave.amplitudes);
        wireframe_program.get_uniform<Wave::Param>("frequencies").set_value(wave.frequencies);
        wireframe_program.get_uniform<Wave::Param>("speeds").set_value(wave.speeds);
        wireframe_program.get_uniform<Wave::Param>("bearings").set_value(wave.bearings);

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glDrawElements(GL_TRIANGLES, element_buffer_data.size(), GL_UNSIGNED_INT, (void*)0);
    }
}

void WaveScene::draw(float secs) { m_impl->draw(m_config, secs); }
