#include "ConfigUI.h"
#include "GraphScene.h"
#include "Random.h"
#include "SDLWindow.h"
#include "WaveScene.h"

#include "GLFramework.h"

int main()
{
    Random::seed(0);

    SDLWindow window("Hello, world!", { 1024, 720 });
    MUST(window.setup_imgui());

    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

    auto& event_loop = window.event_loop();

    ConfigUI config;

    WaveScene wave_scene(config, window.aspect_ratio());
    GraphScene graph_scene(config);
    Scene* current_scene = &wave_scene;

    event_loop.run([&](uint64_t ticks) {
        float secs = ticks / 1000.0f;
        current_scene->draw(secs);
        config.draw();

        if (config.scene_id() == 0)
            current_scene = &wave_scene;
        else
            current_scene = &graph_scene;

        window.draw();
    });

    return 0;
}
