#pragma once

#define GL_GLEXT_PROTOTYPES 1
#ifdef __APPLE__
// make osx complain less about deprecated stuff
#define GL_SILENCE_DEPRECATION 1
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#endif
