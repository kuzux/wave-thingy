#pragma once
#include <cstdint>

namespace GL
{
using uint = uint32_t; // GLuint
using int_ = int32_t; // GLint
using enum_ = uint32_t; // GLenum
using boolean = bool; // GLboolean
}
