#pragma once
#include "ConfigUI.h"
#include "Scene.h"

class GraphScene : public Scene
{
    struct Impl;

public:
    GraphScene(ConfigUI const& config);
    ~GraphScene();

    void draw(float secs) override;

private:
    Impl* m_impl;
    ConfigUI const& m_config;
};
