#version 330 core

layout (location = 0) in vec3 pos;

out vec3 frag_pos;

uniform mat4 projection;
uniform mat4 camera;

void main()
{
    // opengl uses a different coordinate system for models and textures
    frag_pos = vec3(-pos.x, pos.z, pos.y);

    // we want the skybox to rotate with the camera, but not move with it
    mat4 no_translation = mat4(mat3(camera));
    
    gl_Position = projection * no_translation * vec4(pos, 1.0);
}
