#version 330 core
out vec4 color;

in vec3 frag_pos;

uniform samplerCube skybox;

void main()
{    
    color = texture(skybox, frag_pos);
}
