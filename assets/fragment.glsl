#version 330 core
#define NUM_WAVES 40
#define K 5.0

in vec3 frag_pos;

uniform vec3 light_pos;
uniform vec3 camera_pos;
uniform vec3 wave_color;

out vec4 color;

uniform float amplitudes[NUM_WAVES];
uniform float frequencies[NUM_WAVES]; // in Hz
uniform float speeds[NUM_WAVES]; // in Wavelengths per second
uniform float bearings[NUM_WAVES]; // in radians
uniform float time; // in seconds

uniform samplerCube skybox;

#define PI 3.14159265358979323846
#define TAU 6.28318530717958647692

vec3 frag_normal(vec3 pos) {
    // we can compute the normal analytically
    // z(x, y) = \sum_{i} A_i sin^k(\omega_i x_hat + \phi_i t)
    // f = <x, y, \sum_{i} A_i sin^k(\omega_i x_hat + \phi_i t>
    // df/dx = <1, 0, \sum_{i} A_i dx_hat/dx \omega_i k cos^(k-1)(\omega_i x_hat + \phi_i t)>
    // df/dy = <0, 1, \sum_{i} A_i dx_hat/dy \omega_i k cos^(k-1)(\omega_i x_hat + \phi_i t)>

    float x = pos.x;
    float y = pos.y;

    // df/dx
    vec3 tangent = vec3(1.0, 0.0, 0.0);

    // df/dy
    vec3 binormal = vec3(0.0, 1.0, 0.0);

    for(int i = 0; i < NUM_WAVES; i++) {
        float amplitude = amplitudes[i];
        float frequency = frequencies[i];
        float speed = speeds[i];
        float bearing = bearings[i];

        float x_hat = x * cos(bearing) + y * sin(bearing);
        float dx_hat_dx = cos(bearing);
        float dx_hat_dy = sin(bearing);

        float omega = frequency * TAU; // angular frequency
        float phi = speed * omega; // phase constant

        tangent.z += amplitude * omega * dx_hat_dx * K * pow(cos(omega * x_hat + phi * time), K-1.0);
        binormal.z += amplitude * omega * dx_hat_dy * K * pow(cos(omega * x_hat + phi * time), K-1.0);
    }

    return normalize(cross(tangent, binormal));
}

void main() {
    vec3 material_color = vec3(.1, .4, .1);

    float ambient_strength = .3;
    vec3 ambient = ambient_strength * wave_color;

    vec3 light_dir = normalize(light_pos - frag_pos);
    vec3 normal = frag_normal(frag_pos);

    float diffuse_strength = max(dot(normal, light_dir), 0.);

    // reflect off of the skybox
    vec3 camera_dir = normalize(camera_pos - frag_pos);
    vec3 reflected = reflect(-camera_dir, normal);

    // schlick fresnel
    float fresnel_product = abs(dot(normal, camera_dir));
    float fresnel = min(pow(1. - fresnel_product, 5.), 1.0);

    vec3 skybox_color = texture(skybox, reflected).rgb;
    vec3 reflection = fresnel * skybox_color;

    vec3 diffuse = diffuse_strength * wave_color;

    vec3 halfway = normalize(light_dir + camera_dir);
    float specular_strength = pow(max(dot(normal, halfway), 0.), 32.);

    vec3 specular_color = 0.75 * vec3(1., 1., .9) + 0.25 * wave_color;
    vec3 specular = specular_strength * fresnel * specular_color;

    vec3 frag_color = ambient + diffuse + reflection + specular;

    color = vec4(frag_color, 1.0);
}
