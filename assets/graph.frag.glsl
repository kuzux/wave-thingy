#version 330 core

#define NUM_WAVES 40
#define K 5.0

uniform float amplitudes[NUM_WAVES];
uniform float frequencies[NUM_WAVES]; // in Hz
uniform float speeds[NUM_WAVES]; // in Wavelengths per second

uniform float time; // in seconds

#define PI 3.14159265358979323846
#define TAU 6.28318530717958647692

in vec2 frag_uv;
out vec4 color;

void main()
{
    // graph goes from -limits to +limits
    vec2 limits = vec2(5.0, 2.0);

    float x = frag_uv.x*2*limits.x - limits.x;
    float y = frag_uv.y*2*limits.y - limits.y;

    // f(x) = \sum_{i} A_i sin(\omega_i x + \phi_i t)
    float fx = 0.0;
    for(int i = 0; i < NUM_WAVES; i++)
    {
        float amplitude = amplitudes[i];
        float frequency = frequencies[i];
        float speed = speeds[i];

        float omega = frequency * TAU; // angular frequency
        float phi = speed * omega; // phase constant

        fx += amplitude * pow(sin(omega * x + phi * time), K);
    }

    if(abs(fx - y) < 0.01)
        color = vec4(1.0, 0.0, 0.0, 1.0);
    else
        color = vec4(0.0, 0.0, 0.0, 1.0);
}
