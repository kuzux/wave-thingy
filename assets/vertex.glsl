#version 330 core
#define NUM_WAVES 40
#define K 5.0

layout(location = 0) in vec3 pos;

uniform mat4 camera;
uniform mat4 projection;

uniform float amplitudes[NUM_WAVES];
uniform float frequencies[NUM_WAVES]; // in Hz
uniform float speeds[NUM_WAVES]; // in Wavelengths per second
uniform float bearings[NUM_WAVES]; // in radians

uniform float time; // in seconds

// these are passed to the fragment shader for lighting
out vec3 frag_pos;

#define PI 3.14159265358979323846
#define TAU 6.28318530717958647692

void main() {
    vec3 result = pos;

    float x = pos.x;
    float y = pos.y;

    for(int i = 0; i < NUM_WAVES; i++) {
        float amplitude = amplitudes[i];
        float frequency = frequencies[i];
        float speed = speeds[i];
        float bearing = bearings[i];

        float x_hat = x * cos(bearing) + y * sin(bearing); // x in the direction of the wave

        float omega = frequency * TAU; // angular frequency
        float phi = speed * omega; // phase constant

        // z(x, y) = \sum_{i} A_i sin^k(\omega_i (x + y) + \phi_i t)
        result.z += amplitude * pow(sin(omega * x_hat + phi * time), K);
    }

    frag_pos = result;

    gl_Position = projection * camera * vec4(result, 1.0);
}
