#!/usr/bin/env python3

import sys
import os
import platform
import shutil
import glob
import subprocess
import argparse
import time
import json

parser = argparse.ArgumentParser()
parser.add_argument('--release', action="store_true")
parser.add_argument('--run', action="store_true")
parser.add_argument('--vscode-setup', action="store_true")
parser.add_argument('--hooks-setup', action="store_true")
parser.add_argument('--clean', action="store_true")
parser.add_argument('--check-format', action="store_true")
parser.add_argument('--format', action="store_true")
parser.add_argument('--time', action="store_true")

args = parser.parse_args()

clang_format = shutil.which("clang-format")
cxx = shutil.which("g++")
cxx_flags = "-fsanitize=address -Wall -Wextra -Wunused-variable -std=c++17".split()
ld_flags = "-fsanitize=address".split()
executable = "dest/wave"

if args.release:
    cxx_flags += ["-O3"]
    ld_flags += ["-O3"]
else:
    cxx_flags += ["-g"]
    ld_flags += ["-g"]

pkgs = ["fmt", "sdl2", "glm"]

if platform.system() == "Darwin":
    ld_flags += ['-framework', 'OpenGL']
else:
    pkgs += ["opengl"]

# type is one of "cflags" or "libs"
def pkg_config(type, pkgs):
    output = subprocess.check_output(["pkg-config", "--" + type, *pkgs])
    return output.decode('utf-8').split()

cxx_flags += pkg_config("cflags", pkgs)
ld_flags += pkg_config("libs", pkgs)

def run_cmd(args, output=True):
    print("+ ", " ".join(args))
    stdout = None
    stderr = None
    if not output:
        stdout = subprocess.DEVNULL
        stderr = subprocess.DEVNULL
    
    result = subprocess.run(args, stdout=stdout, stderr=stderr)

    if result.returncode != 0:
        cmd_string = " ".join(args)

        print("\x1b[31mCommand \x1b[1;31m", f"\"{cmd_string}\"", f"\x1b[0;31m failed with exit code {result.returncode}", "\x1b[0m")
        raise Exception("Command failed")

def object_file_path(src_path):
    first, rest = src_path.split(os.path.sep, 1)
    if first == "src":
        first = "dest"
    nonext, ext = rest.rsplit(".", 1)
    ext = "o"
    return os.path.join(first, f"{nonext}.{ext}")

def newest_modification_time(paths):
    def modtime(path):
        try:
            return os.path.getmtime(path)
        except OSError:
            return 0

    modtimes = map(modtime, paths)
    return max(modtimes)

def parse_makefile_deps(output_as_bytes):
    out = output_as_bytes.decode('utf-8').replace("\\\n", "").split(": ", 2)
    return out[1].split()

def cpp_source_dependencies(src):
    # TODO: Cache these dependencies as well
    return parse_makefile_deps(subprocess.check_output([cxx, "-M", src, *cxx_flags]))

def should_compile_cpp(src, dest):
    deps = cpp_source_dependencies(src)
    src_time = newest_modification_time(deps)
    dest_time = newest_modification_time([dest])

    return src_time > dest_time

def compile_cpp(srcs):
    objs = []
    for src in srcs:
        dest = object_file_path(src)
        if should_compile_cpp(src, dest):
            os.makedirs(os.path.dirname(dest), exist_ok = True)
            run_cmd([cxx, "-c", src, "-o", dest, *cxx_flags])

        objs.append(dest)
    return objs

def link(objs, dest):
    src_time = newest_modification_time(objs)
    dest_time = newest_modification_time([dest])

    if src_time > dest_time:
        run_cmd([cxx, "-o", dest, *objs, *ld_flags])

# Subcommands

def vscode_setup():
    config = {
        "configurations": [
            {
                "name": "Wave",
                "includePath": [
                    "${workspaceFolder}/**" # To be filled later
                ],
                "defines": [], # To be filled later
                "compilerPath": cxx,
                "cStandard": "c17",
                "cppStandard": "c++17",
                "intelliSenseMode": "macos-gcc-arm64" # TODO: Change this with os/architecture
            }
        ],
        "version": 4
    }
    for flag in cxx_flags:
        if flag.startswith("-I"):
            config["configurations"][0]["includePath"].append(flag[2:])
        elif flag.startswith("-D"):
            config["configurations"][0]["defines"].append(flag[2:])
    
    os.makedirs(".vscode", exist_ok = True)
    with open(".vscode/c_cpp_properties.json", "w") as f:
        json.dump(config, f, indent=4)

def hooks_setup():
    os.makedirs(".git/hooks", exist_ok = True)
    if os.path.exists(".git/hooks/pre-commit"):
        raise Exception("pre-commit hook already exists")
    with open(".git/hooks/pre-commit", "w") as f:
        f.write("#!/bin/sh\n")
        f.write("python3 build.py --check-format\n")
    os.chmod(".git/hooks/pre-commit", 0o755)

def clean():
    shutil.rmtree("dest")

def build(args):
    objs = []
    objs += compile_cpp(glob.glob("src/**/*.cpp", recursive=True))
    link(objs, executable)

    if args.run:
        run_cmd([executable])

def format(check_only=False):
    files_to_format = glob.glob("src/**/*.cpp", recursive=True)
    files_to_format += glob.glob("src/**/*.h", recursive=True)
    files_to_format = [f for f in files_to_format if not f.startswith("src/vendor")]

    if check_only:
        # TODO: Only check changes in the git index
        try:
            run_cmd(["clang-format", "-i", "--dry-run", "--Werror", *files_to_format], output=False)
        except:
            print("Formatting check failed, run `./build.py --format` to auto-format the files", file = sys.stderr)
            raise SystemExit(1)
    else:
        run_cmd(["clang-format", "-i", *files_to_format])

start_time = time.time()

if args.vscode_setup:
    vscode_setup()
elif args.hooks_setup:
    hooks_setup()
elif args.check_format:
    format(check_only=True)
elif args.format:
    format()
elif args.clean:
    clean()
else:
    build(args)

end_time = time.time()

if args.time:
    print(f"Build took {end_time - start_time} seconds")
